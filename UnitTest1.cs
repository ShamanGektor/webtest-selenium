using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
namespace web_test;

public class Tests
{
    private IWebDriver driver;

    public enum Browser
    {
        Chrome,
        Firefox,
        Edge
    }


    public static DriverOptions GetDriverOptions(Browser browser) => browser switch
    {
        Browser.Chrome => new ChromeOptions(),
        Browser.Firefox => new FirefoxOptions(),
        Browser.Edge => new EdgeOptions(),
        _ => throw new ArgumentException("Browser not supported")
    };

    [SetUp]
    public void Setup()
    {   
        var remoteAddress = new Uri("http://192.168.99.125:4444");
        var browserEnv = Environment.GetEnvironmentVariable("Browser");
        Browser browser;
        Enum.TryParse(browserEnv,out browser);
        DriverOptions options = GetDriverOptions(browser);
        driver = new RemoteWebDriver(remoteAddress,options.ToCapabilities());
    }

    [Test]
    public void TestGoogleCalculator()
    {
        driver.Navigate().GoToUrl("https://www.google.com/");
        IWebElement element = driver.FindElement(By.Name("q"));
        element.SendKeys("calculator"+Keys.Return);
        IWebElement button_2 = driver.FindElement(By.CssSelector("div[jsname='lVjWed']"));
        button_2.Click();
        driver.FindElement(By.CssSelector("div[jsname='YovRWb']")).Click();
        button_2.Click();
        driver.FindElement(By.CssSelector("div[jsname='Pt8tGc']")).Click();
        var result = driver.FindElement(By.Id("cwos")).Text;  
        Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
        screenshot.SaveAsFile(Directory.GetCurrentDirectory()+"/screenshots/GoogleCalculatorScreenshot.png",ScreenshotImageFormat.Png);
        Assert.AreEqual("4",result);
    }

    [Test]
    public void TestCalculatorNet()
    {
        driver.Navigate().GoToUrl("https://www.calculator.net/");
        IWebElement button_2 = driver.FindElement(By.XPath("//span[text()='2']"));
        button_2.Click();
        driver.FindElement(By.XPath("//span[text()='×']")).Click();
        button_2.Click();
        driver.FindElement(By.XPath("//span[text()='=']")).Click();
        var result = driver.FindElement(By.Id("sciOutPut")).Text;
        Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
        screenshot.SaveAsFile(Directory.GetCurrentDirectory()+"/screenshots/CalculatorNetScreenshot.png",ScreenshotImageFormat.Png);
        Assert.AreEqual(" 4",result);
    }


    [TearDown]
        public void CleanUp()
        {
            driver.Quit();
        }
}